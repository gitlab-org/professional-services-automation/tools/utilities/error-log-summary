import json

def get_summary(fpath):
        with open(fpath, "r") as f:
                data = json.load(f)
        summary[fpath] = {}
        for d in data:
                if d.get("exception.class", None):
                        exc = d["exception.class"]
                        if summary[fpath].get(exc):
                                summary[fpath][exc] += 1
                        else:
                                summary[fpath][exc] = 1

if __name__ == "__main__":
        summary = {}
        get_summary("logs_archive.json")
        get_summary("logs_current.json")
        print(json.dumps(summary, indent=4))
