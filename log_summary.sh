#!/bin/bash

working_dir=$(pwd)

cd /var/log/gitlab/

## Aggregate currnet logs and archived logs
grep -i 'error' $(find . -name '*json.log' -mtime -4) | sed 's/.*_json\.log:/,/g' > $working_dir/logs_current.json
zgrep -i 'error' $(find . -name '*json.log.*.gz' -mtime -4) | sed 's/.*_json\.log\.[0-9]*\.gz:/,/g' > $working_dir/logs_archive.json

## Clean up JSON files
sed -i '0,/,/s//[/' $working_dir/logs_current.json
echo "]" >> $working_dir/logs_current.json

sed -i '0,/,/s//[/' $working_dir/logs_archive.json
echo "]" >> $working_dir/logs_archive.json

cd $working_dir

## Generate summary
python3 summary.py
