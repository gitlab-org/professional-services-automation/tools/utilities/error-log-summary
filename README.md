# Error Log Summary

## Usage

- Download the bash script and python script onto an omnibus installation
- `sudo ./log_summary.sh`
- Review logs*.json to see all errors occurred
- A summary of exceptions should be printed to STDOUT

## Requirements

- Python 3
- zgrep
- bash
